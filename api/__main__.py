from flask import Flask, redirect
from flask_cors import CORS

from api.controllers.home_controller import home_controller
from api.controllers.blockchain_controller import init_blockchain_controller
from api.controllers.wallet_controller import init_wallet_controller
from api.controllers.transactions_controller import init_transactions_controller
from api.controllers.node_controller import init_node_controller

from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain

from api.services.blockchain_service import BlockchainService
from api.services.wallet_service import WalletService
from api.services.transaction_service import TransactionService
from api.services.node_service import NodeService

from argparse import ArgumentParser

blockchain_service = BlockchainService()
wallet_service = WalletService()
transaction_service = TransactionService()
node_service = NodeService()

parser = ArgumentParser()
parser.add_argument('-p', '--port', type=int, default=5000)
port = parser.parse_args().port
wallet = Wallet(port)
blockchain = Blockchain(wallet.public_key, port)

blockchain_api = Flask(__name__)
blockchain_api.register_blueprint(home_controller)
blockchain_api.register_blueprint(init_blockchain_controller(blockchain, blockchain_service, wallet))
blockchain_api.register_blueprint(init_wallet_controller(blockchain, wallet, wallet_service))
blockchain_api.register_blueprint(init_transactions_controller(blockchain, wallet, transaction_service))
blockchain_api.register_blueprint(init_node_controller(blockchain, wallet, node_service))

CORS(blockchain_api)

if __name__ == '__main__':
    blockchain_api.run(host='0.0.0.0', port=port)


