from flask import jsonify

class HttpResponse:
    @staticmethod
    def get_response(message: str, code: int, body: str,
     error: str = None)-> str:
        return jsonify({
            'message': message,
            'code': code,
            'body': body,
            'error': error,
        }), code