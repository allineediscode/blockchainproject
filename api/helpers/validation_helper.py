from api.helpers.hash_helper import hash_block, hash_string_256
from api.domain.wallet import Wallet

class Validator:
    @classmethod
    def verify_chain(cls, blockchain) -> bool:
        for (index, block) in enumerate(blockchain):
            if index == 0:
                continue
            if block.previous_hash != hash_block(blockchain[index - 1]):
                return False
            if not cls.is_proof_valid(block.transactions[:-1], block.previous_hash, block.proof):
                return False
        return True

    @classmethod
    def validate_transactions(cls, open_transactions, get_balance):
        return all([cls.verify_transaction(tx, get_balance, False) for tx in open_transactions])

    @staticmethod
    def verify_transaction(transaction, get_balance, check_funds = True) -> int:
        if check_funds:
            sender_balance = get_balance(transaction.sender)
            return sender_balance >= transaction.amount and Wallet.verify_transaction(transaction)
        else:
            return Wallet.verify_transaction(transaction)

    @staticmethod
    def is_proof_valid(transactions, last_hash, proof_number) -> str:
        guess = f'{str([transaction.to_ordered_dict() for transaction in transactions])}{str(last_hash)}{str(proof_number)}'.encode()
        guess_hash = hash_string_256(guess)
        return guess_hash[0:2] == '00'
