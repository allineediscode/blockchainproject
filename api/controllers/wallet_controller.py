from flask import Blueprint
from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.services.wallet_service import WalletService

def init_wallet_controller(blockchain: Blockchain, wallet: Wallet,
 service: WalletService) -> Blueprint:
    wallet_controller = Blueprint('wallet', __name__)

    @wallet_controller.route('/wallet/create', methods=['POST'])
    def create_wallet():
        return service.create_wallet(blockchain, wallet)

    @wallet_controller.route('/wallet/login', methods=['GET'])
    def load_wallet():
        return service.load_wallet(blockchain, wallet)

    @wallet_controller.route('/wallet/balance', methods=['GET'])
    def get_balance():
        return service.get_balance(blockchain, wallet)
    
    return wallet_controller
