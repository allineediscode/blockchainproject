from flask import Blueprint, request
from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.services.blockchain_service import BlockchainService

def init_blockchain_controller(blockchain: Blockchain,
 service: BlockchainService, wallet: Wallet) -> Blueprint:
    blockchain_controller = Blueprint('blockchain', __name__)

    @blockchain_controller.route('/blockchain', methods=['GET'])
    def get_blockchain():
        return service.get_blockchain_snapshot(blockchain)

    @blockchain_controller.route('/blockchain/mine', methods=['POST'])
    def mine_block():
        return service.mine_block(blockchain, wallet)

    @blockchain_controller.route('/broadcast-block', methods=['POST'])
    def broadcast_block():
        return service.broadcast_block(blockchain, wallet, request.get_json())
    
    return blockchain_controller
