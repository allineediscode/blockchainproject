from flask import Blueprint, send_from_directory, redirect

home_controller = Blueprint('home', __name__)

@home_controller.route('/home', methods=['GET'])
def home():
    return send_from_directory('views', 'node.html')

@home_controller.route('/network', methods=['GET'])
def network():
    return send_from_directory('views', 'network.html')

@home_controller.route('/')
def go_to_home():
    return redirect('/home', code=302)