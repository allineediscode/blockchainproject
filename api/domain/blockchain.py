import json
import requests
from functools import reduce

from api.helpers.hash_helper import hash_block
from api.helpers.validation_helper import Validator
from api.domain.block import Block
from api.domain.transaction import Transaction
from api.domain.wallet import Wallet

MINING_REWARD = 10

class Blockchain:
    def __init__(self, public_key: str, node_id: str) -> None:
        GENESIS_BLOCK = Block(0, '', [], 100, 0)
        self.chain = [GENESIS_BLOCK]
        self.__open_transactions = []
        self.__peer_nodes = set()
        self.public_key = public_key
        self.node_id = node_id
        self.load_data()

    @property
    def chain(self) -> float:
        return self.__chain[:]

    @chain.setter
    def chain(self, value: float) -> None:
        self.__chain = value

    def get_open_transactions(self) -> list:
        return self.__open_transactions[:]

    def load_data(self) -> None:
        try:
            with open(f'api/data/blockchain-snapshot-{self.node_id}.txt', mode='r') as file:
                # file_content = pickle.loads(file.read())
                file_content = file.readlines()
                # blockchain = file_content['chain']
                # open_transactions = file_content['open_transactions']
                blockchain = json.loads(file_content[0][:-1])
                updated_blockchain = []
                for block in blockchain:
                    converted_transaction = [Transaction(
                        transaction['sender'], transaction['recipient'], transaction['signature'], transaction['amount']) for transaction in
                        block['transactions']]
                    updated_block = Block(block['index'], block['previous_hash'],
                                          converted_transaction, block['proof'], block['timestamp'])
                    updated_blockchain.append(updated_block)
                self.chain = updated_blockchain
                open_transactions = json.loads(file_content[1][:-1])
                updated_transactions = []
                for transaction in open_transactions:
                    updated_transaction = Transaction(
                        transaction['sender'], transaction['recipient'], transaction['signature'], transaction['amount'])
                    updated_transactions.append(updated_transaction)
                self.__open_transactions = updated_transactions
                self.__peer_nodes = set(json.loads(file_content[2]))
        except (IOError, IndexError):
            print('Something')

    def save_data(self) -> None:
        try:
            with open(f'api/data/blockchain-snapshot-{self.node_id}.txt', mode='w') as file:
                saveable_chain = [block.__dict__ for block in [Block(block_el.index, block_el.previous_hash, [
                    transaction.__dict__ for transaction in block_el.transactions], block_el.proof, block_el.timestamp)
                                                               for block_el in self.__chain]]
                file.write(json.dumps(saveable_chain))
                file.write('\n')
                saveable_transactions = [
                    transaction.__dict__ for transaction in self.__open_transactions]
                file.write(json.dumps(saveable_transactions))
                file.write('\n')
                file.write(json.dumps(list(self.__peer_nodes)))
        except IOError:
            print('Saving failed')

    def proof_of_work(self) -> int:
        last_block = self.__chain[-1]
        last_hash = hash_block(last_block)
        proof = 0
        while not Validator.is_proof_valid(self.__open_transactions, last_hash, proof):
            proof += 1
        return proof

    def get_balance(self, sender=None) -> float:
        if sender == None:
            if self.public_key != None:
                participant = self.public_key
        else: 
            participant = sender

        tx_sender = [[transaction.amount for transaction in block.transactions
                    if transaction.sender == participant] for block in self.__chain]
        open_tx_sender = [transaction.amount
                        for transaction in self.__open_transactions if transaction.sender == participant]
        tx_sender.append(open_tx_sender)
        amount_sent = reduce(
            lambda tx_sum, tx_amt: tx_sum + sum(tx_amt) 
            if len(tx_amt) > 0 else int(tx_sum) + 0, tx_sender, 0)

        tx_recipient = [[transaction.amount for transaction in block.transactions
                        if transaction.recipient == participant] for block in self.__chain]
        amount_received = reduce(
            lambda tx_sum, tx_amt: tx_sum + sum(tx_amt) 
            if len(tx_amt) > 0 else int(tx_sum) + 0, tx_recipient, 0)

        return amount_received - amount_sent

    def get_last_blockchain_value(self) -> None:
        if len(self.__chain) < 1:
            return None
        return self.__chain[-1]

    def add_transaction(self, recipient: str, sender: str, signature: str, amount=1.0, is_receiving=False) -> bool:
        transaction = Transaction(sender, recipient, amount, signature)
        print(Validator.verify_transaction(transaction, self.get_balance))
        if Validator.verify_transaction(transaction, self.get_balance):
            self.__open_transactions.append(transaction)
            self.save_data()
            if not is_receiving:
                for node in self.__peer_nodes:
                    url = f'http://{node}/broadcast-transaction'
                    try:
                        response = requests.post(url, json={'sender': sender, 'recipient': recipient, 'amount': amount, 'signature': signature})
                        if response.status_code == 400 or response.status_code == 500:
                            print(f'Transaction declined, needs resolving. from add_transaction {node}')
                            return False
                    except requests.exceptions.ConnectionError:
                        continue
            return True
        return False

    def mine_block(self) -> Block:
        if self.public_key == None:
            return None
        last_block = self.__chain[-1]
        hashed_block = hash_block(last_block)
        proof = self.proof_of_work()
        reward_transaction = Transaction('MINING', self.public_key, MINING_REWARD, '')
        copied_transactions = self.__open_transactions[:]
        for transaction in copied_transactions:
            if not Wallet.verify_transaction(transaction):
                return None
        copied_transactions.append(reward_transaction)
        block = Block(len(self.__chain), hashed_block, copied_transactions, proof)
        self.__chain.append(block)
        self.__open_transactions = []
        self.save_data()
        for node in self.__peer_nodes:
            url = f'http://{node}/broadcast-block'
            converted_block = block.__dict__.copy()
            converted_block['transactions'] = [transaction.__dict__ for transaction in converted_block['transactions']]
            try:
                response = requests.post(url, json={'block': converted_block})
                if response.status_code == 400 or response.status_code == 500:
                    print('Block declined, needs resolving')
            except requests.exceptions.ConnectionError:
                continue
        return block

    def add_block(self, block: Block):
        transactions = [Transaction(transaction['sender'], transaction['recipient'], transaction['amount'], transaction['signature']) for transaction in block['transactions']]
        is_valid = Validator.is_proof_valid(transactions[:-1], block['previous_hash'], block['proof'])
        hashesh_match = hash_block(self.chain[-1]) == block['previous_hash']
        if not is_valid or not hashesh_match:
            return False
        self.__chain.append(Block(block['index'], block['previous_hash'], transactions, block['proof'], block['timestamp']))
        stored_transactions = self.__open_transactions[:]
        for incoming_transactions in block['transactions']:
            for open_transactions in stored_transactions:
                if (open_transactions.sender == incoming_transactions['sender'] 
                and open_transactions.recipient == incoming_transactions['recipient'] 
                and open_transactions.amount == incoming_transactions['amount'] 
                and open_transactions.signature == incoming_transactions['signature']):
                    try:
                        self.__open_transactions.remove(open_transactions)
                    except ValueError:
                        print('Item was already removed.')
        self.save_data()
        return True
    
    def change_wallet(self, guid: str) -> None:
        self.public_key = guid

    def add_peer_node(self, node):
        self.__peer_nodes.add(node)
        self.save_data()

    def remove_peer_node(self, node):
        self.__peer_nodes.discard(node)
        self.save_data()

    def get_peer_nodes(self):
        return list(self.__peer_nodes)
