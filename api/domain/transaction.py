from collections import OrderedDict
from api.domain.printable import Printable


class Transaction(Printable):
    def __init__(self, sender: str, recipient: str,
                 amount: float, signature: str) -> None:
        self.sender = sender
        self.recipient = recipient
        self.amount = amount
        self.signature = signature

    def __repr__(self):
        return str(self.__dict__)

    def to_ordered_dict(self):
        return OrderedDict(
            [('sender', self.sender), ('recipient', self.recipient), ('amount', self.amount)])
