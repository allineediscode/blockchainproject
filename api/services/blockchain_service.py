from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.helpers.response_helper import HttpResponse

class BlockchainService:
    def __init__(self):
        pass

    def get_blockchain_snapshot(self, blockchain: Blockchain) -> str:  
        converted_chain = [block.__dict__.copy() for block in blockchain.chain]

        for block in converted_chain:
            block['transactions'] = [transaction.__dict__ for transaction in block['transactions']]

        return HttpResponse.get_response('Success', 200, converted_chain)

    def mine_block(self, blockchain: Blockchain, wallet: Wallet) -> str:
        block = blockchain.mine_block()
        if block != None:
            dict_block = block.__dict__.copy()
            dict_block['transactions'] = [transaction.__dict__ for transaction in dict_block['transactions']]
            return HttpResponse.get_response('Success', 201, {'block': dict_block, 'balance': blockchain.get_balance()})
        return HttpResponse.get_response('Failure', 400, 'Adding a block failed.', f'Is wallet set up: {wallet.public_key != None}')

    def broadcast_block(self, blockchain: Blockchain, wallet: Wallet, request: dict) -> str:
        required = ['block']
        if not request or not all(key in request for key in required):
             return HttpResponse.get_response('Failure', 409, 'Something went wrong', 'Invalid data provided.')
        block = request['block']
        if block['index'] == blockchain.chain[-1].index + 1:
            if blockchain.add_block(block):
                return HttpResponse.get_response('Success', 201, 'Block broadcasted.')
            return HttpResponse.get_response('Failure', 500, 'Something went wrong.', 'Broadcasting block failed')
            
        elif block['index'] > blockchain.chain[-1].index:
            pass
        return HttpResponse.get_response('Failure', 409, 'Something went wrong', 'Broadcasting block failed.')
