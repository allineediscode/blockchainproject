from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.helpers.response_helper import HttpResponse

class WalletService:
    def __init__(self):
        pass

    def create_wallet(self, blockchain: Blockchain, wallet: Wallet) -> str:  
        wallet.create_key()
        if wallet.save_keys():
            blockchain.change_wallet(wallet.public_key)
            return HttpResponse.get_response('Success', 201, {'public_key': wallet.public_key, 'private_key': wallet.private_key, 'balance': blockchain.get_balance()})
        return HttpResponse.get_response('Failure', 400, 'Something went wrong.', 'Wallet creation failed.')

    def load_wallet(self, blockchain: Blockchain, wallet: Wallet)-> str:
        if wallet.load_key():
            blockchain.change_wallet(wallet.public_key)
            return HttpResponse.get_response('Success', 200, {'public_key': wallet.public_key, 'private_key': wallet.private_key, 'balance': blockchain.get_balance()})
        return HttpResponse.get_response('Failure', 400, 'Something went wrong.', 'Loading wallet failed.')

    def get_balance(self, blockchain: Blockchain, wallet: Wallet)-> str:
        balance = blockchain.get_balance()
        if balance != None:
            return HttpResponse.get_response('Success', 200, {'public_key': wallet.public_key, 'balance': balance})
        return HttpResponse.get_response('Failure', 400, 'Something went wrong.', "We could not get your account's balance.")

        