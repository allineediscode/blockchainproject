from api.domain.wallet import Wallet
from api.domain.blockchain import Blockchain
from api.helpers.response_helper import HttpResponse

class NodeService:
    def __init__(self):
        pass

    def add_node(self, blockchain: Blockchain, wallet: Wallet, request: str) -> str:
        if not request or 'node' not in request:
           return HttpResponse.get_response('Failure', 400, 'Adding node failed.', 'Invalid data provided.')
        blockchain.add_peer_node(request.get('node'))
        return HttpResponse.get_response('Success', 201, {'nodes': blockchain.get_peer_nodes()})

    def remove_node(self, blockchain: Blockchain, node_url: str) -> str:
        if node_url == '' or node_url == None:
           return HttpResponse.get_response('Failure', 400, 'Removing node failed.', 'Node not found.')
        blockchain.remove_peer_node(node_url)
        return HttpResponse.get_response('Success', 204, {'nodes': blockchain.get_peer_nodes()})

    def get_all_nodes(self, blockchain: Blockchain) -> str:
        return HttpResponse.get_response('Success', 200, {'nodes': blockchain.get_peer_nodes()})
        
