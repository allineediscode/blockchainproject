names = ['Jack', 'Annie', 'John', 'Lilie', 'Greg', 'Josh', 'Tommy', 'Krieg']

for name in names:
    if len(name) > 4 and ('n' in name or 'N' in name):
        print(name)
        print(len(name))

while len(names) >= 1:
    print(names.pop())

print(names)