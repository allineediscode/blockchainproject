#1
def transform_data(func):
    print(func(10))

# transform_data(lambda data: data / 10)

def transformWithInfiniteParameters(func, *arguments):
    for argument in arguments:
        print(func(argument))

# transformWithInfiniteParameters(lambda data: data / 5, 10, 15, 20, 30)


def transformDataWithFormatting(func, *arguments):
    for argument in arguments:
        print(f'Results: {func(argument):^20.2f}')

transformDataWithFormatting(lambda data: data / 10, 100, 1000, 20, 312)