first_name = input("Your name is: ")
age = int(input('Your age is: '))

def print_user_data():
    print(first_name + ' ' + str(age))

def print_all_data(name='not provided', age='not provided'):
    print(name + age)

def print_decade_user_lived_in():
    print(age // 10)

print_user_data()
print_all_data()
print_decade_user_lived_in()